# Contributing to GitLab Development Kit

This document describes how to contribute code to GitLab Development Kit (GDK).

## Contributing new GDK features

Contribute new features to GDK by:

- Creating a new GDK command.
- Adding a new service for GDK to manage.
- Adding a new capability to existing GDK commands, setup, or service.

### Creating a new GDK command

GDK commands manage operations that affect the entire GDK environment. GDK commands should:

- Perform actions that impact multiple services or the overall GDK setup.
- Not be specific to a single service (for example, database migrations).

Existing `gdk` commands include:

- `gdk start` and `gdk stop` to start and stop all services.
- `gdk update` to update your GDK environment.
- `gdk doctor` to verify the overall health of your GDK setup.

Don't create a GDK command to do things such as:

- Populating data for a single service.
- Performing setup tasks specific to one service.
- Running database migrations for a single service.

Instead, use a Rake task in these cases because they are more appropriate for service-specific actions.

### Creating a new GDK-managed service or Rake task

Use Rake tasks for service-specific actions.

Don't create new service-specific Makefiles (for example, `Makefile.<service name>.mk`) because we're moving individual
Ruby services. Services are stored in `lib/gdk/services`.

Each Ruby service can implement hooks for:

- Setup
- Installation
- Updates

For complex setup procedures:

1. Create a dedicated Rake task.
1. Execute it as part of the setup hook.

When you create new GDK commands or Rake tasks, follow the existing patterns in the GDK codebase.

For more information about this ongoing work, see <https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/166>.
