# frozen_string_literal: true

RSpec.describe GDK::ConfigHelper do
  let(:gdk_root) { Pathname.new('/home/git/gdk') }
  let(:config) { instance_double(GDK::Config, gdk_root: gdk_root) }
  let(:version_path) { 'VERSION' }
  let(:full_path) { gdk_root.join(version_path) }

  before do
    allow(gdk_root).to receive(:join).with(version_path).and_return(full_path)
  end

  describe '.version_from' do
    context 'when version file exists' do
      before do
        allow(full_path).to receive(:exist?).and_return(true)
      end

      context 'when version file contains a commit hash' do
        it 'returns the commit hash' do
          commit_hash = 'a' * 40
          allow(full_path).to receive(:read).and_return(commit_hash)
          expect(described_class.version_from(config, version_path)).to eq(commit_hash)
        end
      end

      context 'when version file contains a short version' do
        it 'returns the version prefixed with v' do
          allow(full_path).to receive(:read).and_return("1.2.3\n")
          expect(described_class.version_from(config, version_path)).to eq('v1.2.3')
        end
      end
    end

    context 'when version file does not exist' do
      before do
        allow(full_path).to receive(:exist?).and_return(false)
      end

      it 'returns an empty string' do
        expect(described_class.version_from(config, version_path)).to eq('')
      end
    end
  end
end
