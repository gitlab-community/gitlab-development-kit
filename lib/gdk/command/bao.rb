# frozen_string_literal: true

module GDK
  module Command
    # Configures openbao client
    class Bao < BaseCommand
      def run(args = [])
        unless GDK.config.openbao.enabled?
          GDK::Output.warn('OpenBao is not enabled. See doc/howto/openbao.md for getting started with OpenBao.')
          return false
        end

        case args.pop
        when 'configure'
          GDK::OpenBao.new.configure
        else
          GDK::Output.warn('Usage: gdk bao configure')
          false
        end
      end
    end
  end
end
