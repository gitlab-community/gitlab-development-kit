# frozen_string_literal: true

require 'fileutils'

module GDK
  module Command
    class Help < BaseCommand
      # Allow invalid gdk.yml.
      def self.validate_config?
        false
      end

      def run(_ = [])
        GDK::Logo.print
        GDK::Output.puts(File.read(GDK.root.join('HELP')))

        true
      end
    end
  end
end
