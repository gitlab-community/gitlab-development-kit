# shellcheck shell=bash
FROM ubuntu:22.04
LABEL authors.maintainer "GDK contributors: https://gitlab.com/gitlab-org/gitlab-development-kit/-/graphs/main"

## The CI script that build this file can be found under: support/docker

ENV DEBIAN_FRONTEND=noninteractive
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV PATH="/home/gdk/.local/bin:/home/gdk/.local/share/mise/bin:/home/gdk/.local/share/mise/shims:${PATH}"
ENV TOOL_VERSION_MANAGER=mise

RUN apt-get update && apt-get install -y curl libssl-dev locales locales-all pkg-config software-properties-common sudo \
    && add-apt-repository ppa:git-core/ppa -y

RUN useradd --user-group --create-home --groups sudo gdk
RUN echo "gdk ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/gdk_no_password

WORKDIR /home/gdk/tmp
RUN chown -R gdk:gdk /home/gdk

USER gdk
COPY --chown=gdk . .

SHELL ["/bin/bash", "-c"]

# Perform bootstrap
# Remove unneeded packages
# Remove files copied during the build process
# Remove build caches
# Note: We cannot remove all of "$HOME/gdk/gitaly/_build/*" because we need to keep the compiled binaries in "$HOME/gdk/gitaly/_build/bin"
#
RUN ./support/bootstrap \
    && sudo apt-get purge software-properties-common -y \
    && sudo apt-get clean -y \
    && sudo apt-get autoremove -y \
    && sudo rm -rf /home/gdk/tmp \
    && sudo rm -rf /var/cache/apt/* /var/lib/apt/lists/* "$HOME/gdk/gitaly/_build/deps/git/source" "$HOME/gdk/gitaly/_build/deps/libgit2/source" "$HOME/gdk/gitaly/_build/cache" "$HOME/gdk/gitaly/_build/deps" "$HOME/gdk/gitaly/_build/intermediate" "$HOME/.cache/" /tmp/*

WORKDIR /home/gdk
