---
stages:
  - build
  - test
  - analyze
  - pre-verify
  - integration
  - verify
  - deploy
  - cleanup

.default_variables: &default_variables
  CI_API_V4_URL: https://gitlab.com/api/v4
  DEFAULT_IMAGE_WITHOUT_TAG: "${CI_REGISTRY_IMAGE}/${TOOL_VERSION_MANAGER}-bootstrapped-verify"
  DEFAULT_BRANCH_IMAGE: "${DEFAULT_IMAGE_WITHOUT_TAG}:${CI_COMMIT_REF_SLUG}"
  DEFAULT_MAIN_IMAGE: "${DEFAULT_IMAGE_WITHOUT_TAG}:main"
  VERIFY_IMAGE_MAIN: "${CI_REGISTRY_IMAGE}/${TOOL_VERSION_MANAGER}-bootstrapped-gdk-installed:main"
  VERIFY_IMAGE: "${CI_REGISTRY_IMAGE}/${TOOL_VERSION_MANAGER}-bootstrapped-gdk-installed:${CI_COMMIT_REF_SLUG}"
  INTEGRATION_IMAGE: "${CI_REGISTRY_IMAGE}/${TOOL_VERSION_MANAGER}-ci-integration:${CI_COMMIT_REF_SLUG}"
  GITLAB_CI_CACHE_DIR: .gitlab-ci-cache
  GITLAB_CI_CACHE_FULL_DIR: $CI_PROJECT_DIR/$GITLAB_CI_CACHE_DIR
  GITLAB_CI_CACHE_FULL_GO_DIR: $GITLAB_CI_CACHE_FULL_DIR/go
  GITLAB_CI_CACHE_GO_DIR: $GITLAB_CI_CACHE_DIR/go
  GITLAB_REMOTE_WORKSPACE_IMAGE: gitlab-remote-workspace
  GITPOD_WORKSPACE_IMAGE: gitpod-workspace
  GDK_INTERNAL_CACHE_FULL_DIR: /home/gdk/$GITLAB_CI_CACHE_DIR
  GDK_INTERNAL_CACHE_RUBY_FULL_DIR: $GDK_INTERNAL_CACHE_FULL_DIR/ruby
  GDK_INTERNAL_CACHE_GO_FULL_DIR: $GDK_INTERNAL_CACHE_FULL_DIR/go
  BUNDLE_PATH: "vendor/bundle"
  BUNDLE_FROZEN: "true"
  BUNDLE_JOBS: "$(nproc)"
  ENABLE_BOOTSNAP: "false"
  PUMA_SINGLE_MODE: "true"
  GDK_DEBUG: "true"
  NOKOGIRI_LIBXML_MEMORY_MANAGEMENT: "default"
  GITLAB_LAST_VERIFIED_SHA_PATH: gitlab-last-verified-sha.json
  REGISTRY_HOST: "registry.gitlab.com"
  REGISTRY_GROUP: "gitlab-org"
  RUBY_VERSION: "3.2"
  FF_TIMESTAMPS: true
  FF_USE_INIT_WITH_DOCKER_EXECUTOR: true
  TOOL_VERSION_MANAGER: "asdf"
  RUBYOPT: "--yjit"
  HUGO_VERSION: "0.145.0"

variables:
  <<: *default_variables

default:
  timeout: 3h
  image: ${DEFAULT_BRANCH_IMAGE}
  tags:
    - gitlab-org

.default-before_script:
  before_script:
    - sysctl -n -w fs.inotify.max_user_watches=524288 || true
    - |
      if [ -f support/ci/utils.sh ]; then
        . support/ci/utils.sh;
        display_debugging || true;
      else
        true;
      fi

.default-after_script:
  after_script:
    # In `after_script`, the working directory always starts at `CI_PROJECT_DIR`.
    - mkdir -p gitlab_log gdk_log
    - mv /home/gdk/gdk/gitlab/log/*.log gitlab_log/
    - mv /home/gdk/gdk/log/ gdk_log/

.cached_variables: &cached_variables
  BUNDLE_PATH: $GDK_INTERNAL_CACHE_RUBY_FULL_DIR/bundle
  GEM_HOME: $GDK_INTERNAL_CACHE_RUBY_FULL_DIR/gem
  GEM_PATH: $GDK_INTERNAL_CACHE_RUBY_FULL_DIR/gem
  GOCACHE: $GDK_INTERNAL_CACHE_GO_FULL_DIR/build
  GOMODCACHE: $GDK_INTERNAL_CACHE_GO_FULL_DIR/mod
  NODE_PATH: $GDK_INTERNAL_CACHE_FULL_DIR/nodejs

.verify-job-cached_variables:
  variables:
    <<: *default_variables
    <<: *cached_variables

.cached-job:
  variables:
    <<: *default_variables
    <<: *cached_variables
  # (Temporarily) disabling the cache.
  # See https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/1886 for more details.
  #
  # cache:
  #   - key:
  #       files:
  #         - '.tool-versions'
  #     paths:
  #       - "$GITLAB_CI_CACHE_DIR"
  #     policy: pull-push

include:
  - template: Workflows/MergeRequest-Pipelines.gitlab-ci.yml
  - local: .gitlab/ci/_rules.gitlab-ci.yml
  - local: .gitlab/ci/_docker.gitlab-ci.yml
  - local: .gitlab/ci/_interruptible.yml
    rules:
      - if: '$CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG == null'
  - local: '.gitlab/ci/_versions.gitlab-ci.yml'
  - local: .gitlab/ci/build.gitlab-ci.yml
  - local: .gitlab/ci/test.gitlab-ci.yml
  - local: .gitlab/ci/analyze.gitlab-ci.yml
  - local: .gitlab/ci/pre-verify.gitlab-ci.yml
  - local: .gitlab/ci/integration.gitlab-ci.yml
  - local: .gitlab/ci/verify.gitlab-ci.yml
  - local: .gitlab/ci/deploy.gitlab-ci.yml
  - local: .gitlab/ci/compile.gitlab-ci.yml
